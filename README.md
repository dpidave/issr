# Scripts to generate dendrogram from ISSR data  
__Requires__  
-  NIX system (ubuntu-like)  
-  phylip  
-  python2  
-  python pandas  
-  R packages ape, vegan  

__Branches__  
-  master: main analysis  
-  phylip:  parsimony testing  
-  bootstrap: bootstrap testing  

__Input__  
As input requires a CSV file in the following format (note header required!):  
```
,lane1,lane2,lane3,lane4,lane5,lane6,lane7
indiv1,1,1,0,0,1,0,1
indiv2,1,1,0,0,1,0,1
indiv3,0,0,1,1,1,0,0
indiv4,0,0,1,1,1,0,0
indiv5,0,0,0,1,1,0,0
```

## Notes  
Missing data is handled by na.rm=T via a vegan method  

## Testing  
A dataset from Szenejko et al 2017 (doi: 10.7717/peerj.2489), was able to
replicate ISSR tree shown in Figure 5.  

The binary data frame is located in the file:
```testing/journal.data.fixed.csv```.  

## Distance methods

1\. Read in data using the openxlsx package with scripts/read.R  
```
Rscript analysis/scripts/read.R data.xlsx
```
The resulting csv file will be in ```raw_data/```

2\.  Process the CSV file through the fix_names.py script to make sure
    that the name is 10 spaces long as required by phylip  

```
python2 analysis/scripts/fix_names.py raw_data/infile.csv
```

This will output a csv file with 'fixed' appended to the file name.  

3\.  Calculate the Dice coefficient distances (equivalent to vegdist bray
option), specific paramater to handle missing data.  

```
Rscript analysis/scripts/get_dice_dist.R raw_data/infilefixed.csv > infile
```

4\.  Modify the infile by replacing the column headings with the number of indiv  
```
           indiv1    
indiv2      0.0000000  
indiv3      0.0000000  0.0000000  
indiv4      0.8451543  0.8451543  0.0000000  
```  
Becomes..  
```
4
indiv1
indiv2      0.0000000 
indiv3      0.8451543  0.8451543  
indiv3      0.8451543  0.8451543  0.8451543
```

5\.  Process the ```infile``` using phylip.  
```
phylip neighbor
```
Select "N" for UPGMA method  
Select "L" for lower triangle  
Press "Y" to run  

The final file will be outfile and outtree.  
```
cat outtree 
((indiv1:0.00000,indiv2:0.00000):0.41780,((indiv3:0.00000,
indiv4:0.00000):0.22361,indiv5:0.22361):0.19419);

cat outfile
Neighbor-Joining/UPGMA method version 3.696


 UPGMA method

 Negative branch lengths allowed


                           +indiv1    
  +------------------------1 
  !                        +indiv2    
--4 
  !                        +indiv3    
  !           +------------2 
  +-----------3            +indiv4    
              ! 
              +------------indiv5    


From     To            Length          Height
----     --            ------          ------
   4        1          0.41780         0.41780
   1     indiv1        0.00000         0.41780
   1     indiv2        0.00000         0.41780
   4        3          0.19419         0.19419
   3        2          0.22361         0.41780
   2     indiv3        0.00000         0.41780
   2     indiv4        0.00000         0.41780
   3     indiv5        0.22361         0.41780
```

## Drawing plots  
1\.  PCoA plots  
```
Rscript analysis/scripts/anal.R  
```

2\.  Dotplot  
```
Rscript analysis/scripts/plot.R  
```

3\.  Dendrogram  
```
Rscript analysis/scripts/draw_tree.R distance/outtree1  
```

## Parsimony methods phylip mix (with Wagner method)  
See Archibald et al 2006 for debate about the methods of ISSR analysis using
parsimony and distance methods. It is worth nothing that ISSR markers are
dominant so the use of Restriction Enzyme based methods (RFLP) is questionable
given these are co-dominant markers.  

## Session info
```
> sessionInfo()
R version 3.4.3 (2017-11-30)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: Ubuntu 16.04.3 LTS

Matrix products: default
BLAS: /usr/lib/libblas/libblas.so.3.6.0
LAPACK: /usr/lib/lapack/liblapack.so.3.6.0

locale:
 [1] LC_CTYPE=en_NZ.UTF-8       LC_NUMERIC=C              
 [3] LC_TIME=en_NZ.UTF-8        LC_COLLATE=en_NZ.UTF-8    
 [5] LC_MONETARY=en_NZ.UTF-8    LC_MESSAGES=en_NZ.UTF-8   
 [7] LC_PAPER=en_NZ.UTF-8       LC_NAME=C                 
 [9] LC_ADDRESS=C               LC_TELEPHONE=C            
[11] LC_MEASUREMENT=en_NZ.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
[1] openxlsx_4.0.17 readxl_1.0.0    vegan_2.4-6     lattice_0.20-35
[5] permute_0.9-4   ape_5.0        

loaded via a namespace (and not attached):
 [1] MASS_7.3-48      compiler_3.4.3   Matrix_1.2-12    tools_3.4.3     
 [5] parallel_3.4.3   mgcv_1.8-23      Rcpp_0.12.15     cellranger_1.1.0
 [9] nlme_3.1-131     grid_3.4.3       cluster_2.0.6  
```
