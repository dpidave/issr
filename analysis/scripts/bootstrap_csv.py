#!/usr/bin/env python2
from pandas import DataFrame
import pandas as pd
import sys
from fix_names import fix_name
# Take a dataframe and resample columns WITH replacement

# usage: python2 bootstrap_csv.py infile.csv interations

#interations: number of datasets to return, default 100
fname = sys.argv[1]
if len(sys.argv) == 2:
    iterations = 100
else:
    iterations = int(sys.argv[2])

# make a dataframe from the csv file
df = DataFrame(pd.read_csv(fname, index_col=0))

counter = 0
while counter != iterations:
    # sample 100% of dataframe, sample with replacement, sample columns
    tmp=df.ix[:,3:].sample(frac=1.0, replace=True, axis=1)
    new_df = df.ix[:,:3].join(tmp)
    new_names = [fix_name(name) for name in new_df.index]
    new_df.index = new_names
    new_df.to_csv('outfile%s.csv'%(str(counter + 1)),header=False)
    print(new_df.head())
    counter += 1
print('Data saved to outfileN.csv where N is the iteration')
