#!/usr/bin/env python2

from pandas import DataFrame
import pandas as pd
import sys


def fix_name(name):
    '''short function to pad name by 10 spaces as req by phylip'''
    name = name.strip()
    while len(name) < 10:
        name = name + ' '
    return name

fname = sys.argv[1]
outfile = fname.split('.csv')[0] + 'fixed.csv'

df = DataFrame(pd.read_csv(fname, index_col=0))
new_names = [fix_name(name) for name in df.index]
df.index = new_names
df.to_csv(outfile, header=False)
